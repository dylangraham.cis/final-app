import React, { useEffect } from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import { toast } from "react-toastify";
import { useHistory } from "react-router-dom";
import { validationSchema } from "../../utills/validation";

const LoginPage = (props) => {
  //initialValues for formik
  const history = useHistory();

  useEffect(() => {
    const hasAuthAccessToken = localStorage.getItem("token");
    if (hasAuthAccessToken) {
      history.push("/users/add");
    }
  }, []);

  const initialValues = {
    email: "devil@gmail.com",
    password: "dev@123",
  };

  const toastData = {
    position: "top-center",
    autoClose: 4000,
    hideProgressBar: false,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    progress: undefined,
  };

  const data = localStorage.getItem("email");

  //handleSubmit condition for form login and reset form

  const handleSubmit = (values, { resetForm }) => {
    if (
      values.email === "devil@gmail.com" &&
      values.password === "dev@123"
    ) {
      var rand = function () {
        return Math.random().toString(36).substr(2); // remove `0.`
      };

      var token = function () {
        return rand() + rand(); // to make it longer
      };
      // console.log(token());
      resetForm();
      localStorage.setItem("token", token());
      const hasAuthAccessToken = localStorage.getItem("token");
      if (hasAuthAccessToken) {
        history.push("/users/add");
      }
      toast.success("logged in successfully", {
        toastData,
      });
    } else {
      toast.error("Incorrect Email or Password!", {
        toastData,
      });
    }
  };

  return (
    <div className="Main-Login">
      <div className="container">
        <Formik
          initialValues={initialValues}
          validationSchema={validationSchema}
          onSubmit={handleSubmit}
        >
          <div className="form_utils">
            <Form className="login-form">
              <h3>Login</h3>
              <label>
                Email: <Field type="email" name="email" />
                <ErrorMessage name="email" component="div" className="error" />
              </label>
              <label>
                Password:
                <Field type="password" name="password" />
                <ErrorMessage
                  name="password"
                  component="div"
                  className="error"
                />
              </label>
              <button className="btn_login" type="submit">
                Submit
              </button>
            </Form>
          </div>
        </Formik>
      </div>
    </div>
  );
};

export default LoginPage;
