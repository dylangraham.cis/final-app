import React, { useState } from "react";
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";
import { deleteUser } from "../../redux/action/deleteUser";
import { useDispatch } from "react-redux";
import { userList } from "../../redux/action/userList";
import { toast } from "react-toastify";

const DeleteModel = (props) => {
  console.log("ddffddfdf", props);
  return (
    <div>
      <Modal
        isOpen={props.show}
        //  toggle={function noRefCheck() {deleteHandler}}
      >
        <Modal
          show={props.show}
          onHide={props.handleClose}
          animation={false}
        ></Modal>
        <ModalHeader toggle={props.handleShow}>User delete</ModalHeader>
        <ModalBody>Are you sure to do this.</ModalBody>
        <ModalFooter>
          <Button onClick={props.userDelete} className="userDelete">
            Yes
          </Button>
          <Button onClick={props.handleClose} className="cancel">
            Cancel
          </Button>
        </ModalFooter>
      </Modal>
    </div>
  );
};

export default DeleteModel;
