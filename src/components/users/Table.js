import React, { useEffect, useState } from "react";
import { userList } from "../../redux/action/userList";
import { updateList } from "../../redux/action/editUser";
import { deleteUser } from "../../redux/action/deleteUser";
import { useDispatch, useSelector } from "react-redux";
import ReactTable from "react-table-6";
import { useHistory } from "react-router-dom";
import "react-confirm-alert/src/react-confirm-alert.css"; // Import css
import { toast } from "react-toastify";
import Navbar from "../common/Navbar";
import Footer from "../common/Footer";
import DeleteModel from "./DeleteModel";

const History = (props) => {
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const [id, setId] = useState();

  const history = useHistory();
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(userList());
  }, []);

  const submitEdit = (data) => {
    history.push({ pathname: "/users/add", state: data });
  };

  const toastData = {
    position: "top-center",
    autoClose: 4000,
    hideProgressBar: false,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    progress: undefined,
  };

  const userDelete = () => {
    const actionToDelete = id;
    dispatch(deleteUser(actionToDelete)).then(() => dispatch(userList()));
    handleClose();
    toast.error("Delete user from List", {
      toastData,
    });
  };

  const deleteHandler = (id) => {
    setId(id);
    handleShow();
  };

  const Table = [
    {
      Header: "Full Name",
      accessor: "firstName",
      Cell: (props) => (
        <div className="number">
          {props.original.firstName}
          {props.original.lastName}
        </div>
      ), // Custom cell components!
    },

    {
      Header: "Email",
      accessor: "email", // String-based value accessors!
    },
    {
      Header: "Select Option",
      accessor: "selectOption",
    },
    {
      Header: "Actions",

      Cell: (props) => (
        <span>
          <button
            className="number"
            onClick={() => {
              submitEdit(props.original);
            }}
          >
            {props.value}
            Edit
          </button>

          <button
            style={{ marginLeft: "10px" }}
            className="number"
            onClick={() => {
              deleteHandler(props.original.id);
            }}
          >
            {props.value}
            delete
          </button>
        </span>
      ),
    },
  ];

  const {
    //add
    getUserDataIsLoading = false,
    userDataList = null,
    userDataError,
  } = useSelector((state) => ({
    getUserDataIsLoading: state.userList.isLoading,
    userDataList: state.userList?.data,
    userDataError: state.userList.error,
  }));

  return (
    <div style={{ backgroundColor: "antiquewhite" }}>
      <Navbar />
      <div className="container">
        <ReactTable
          data={userDataList?.data}
          columns={Table}
          className="ReactTable"
        />
      </div>
      <DeleteModel
        show={show}
        deleteHandler={deleteHandler}
        userDelete={userDelete}
        handleClose={handleClose}
        handleShow={handleShow}
      />
      <Footer />
    </div>
  );
};

export default History;
