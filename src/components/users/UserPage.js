import React, { useEffect, useState } from "react";
import { Formik, Form, Field } from "formik";
import { TextField } from "../common/TextField";
import { addUser } from "../../redux/action/addUser";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import { userList } from "../../redux/action/userList";
import { updateList } from "../../redux/action/editUser";
import { useHistory } from "react-router-dom";
import { validate } from "../../utills/validation";
import SelectOptions from "../common/SelectOptions";
import "react-table-6/react-table.css";
import { useLocation } from "react-router-dom/cjs/react-router-dom.min";
import { toast } from "react-toastify";
import Navbar from "../common/Navbar";
import Footer from "../common/Footer";
import { selectOptions } from "@testing-library/user-event/dist/select-options";

const UserPage = (props) => {
  const [editData, setEditData] = useState("");

  const history = useHistory();

  const location = useLocation();

  useEffect(() => {
    const hasAuthAccessToken = localStorage.getItem("token");
    setEditData(props.location.state);

    if (!hasAuthAccessToken) {
      history.push("/");
    }
  }, []);

  const dispatch = useDispatch();

  // dispatch(userListAction())Runs only on the first render

  useEffect(() => {
    dispatch(userList());
  }, []);

  const [selectedOption, setSelectedOption] = useState(null);

  const Options = [
    { value: "d", label: "Crest" },
    { value: "e", label: "Monday" },
    { value: "v", label: "Sat-Sun" },
  ];

  // get 1 more field in form as hidden value named id
  // and populate this filed with actual id from API
  // now if id is null means we've to create record else just edit the record
  // once that is done just refresh it with dispatch userListAction
  //updateList for edit data link
  //addUser for add user data link
  //userListAction for listing user data link

  const formSubmit = (values, { resetForm }) => {
    const actionToDispatch = values.id
      ? updateList(values.id, values)
      : addUser(values);
    toast.success("Add user successfully", {
      toastData,
    });
    dispatch(actionToDispatch).then(() => dispatch(userList()));
    resetForm();
    setEditData("");
    history.push("/users");
  };

  const toastData = {
    position: "top-center",
    autoClose: 4000,
    hideProgressBar: false,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    progress: undefined,
  };

  return (
    <div className="main">
      <Navbar />
      <div className="container">
        <Formik
          initialValues={{
            firstName: editData?.firstName ? editData?.firstName : "",
            lastName: editData?.lastName ? editData?.lastName : "",
            email: editData?.email ? editData?.email : "",
            password: editData?.password ? editData?.password : "",
            confirmPassword: editData?.confirmPassword
              ? editData?.confirmPassword
              : "",
            selectOption: editData?.selectOption ? editData?.selectOption : "",
            id: editData?.id || null,
          }}
          validationSchema={validate}
          onSubmit={formSubmit}
          enableReinitialize
        >
          {({ values, resetForm, handleChange, handleSubmit }) => {
            return (
              <Form onSubmit={handleSubmit} className="Form-formik">
                <Field
                  type="hidden"
                  className="form-control"
                  name="hiddenField"
                  values={values?.id}
                  className="formik-field"
                />
                <TextField
                  value={values?.firstName}
                  onChange={handleChange}
                  label="First Name"
                  name="firstName"
                  type="text"
                />
                <TextField
                  value={values?.lastName}
                  onChange={handleChange}
                  label="Last Name"
                  name="lastName"
                  type="text"
                />
                <TextField
                  value={values?.email}
                  onChange={handleChange}
                  label="Email"
                  name="email"
                  type="email"
                />
                <TextField
                  value={values?.password}
                  onChange={handleChange}
                  label="Password"
                  name="password"
                  type="password"
                />
                <TextField
                  value={values?.confirmPassword}
                  onChange={handleChange}
                  label="Confirm Password"
                  name="confirmPassword"
                  type="password"
                />
                <SelectOptions
                  value={values?.SelectOption}
                  onChange={handleChange}
                  control="select"
                  label="Select Department"
                  name="selectOption"
                  options={Options}
                />
                <button
                  className="btn-register"
                  type="submit"
                  onClick={handleSubmit}
                >
                  {editData ? "Update" : "Register"}
                </button>
              </Form>
            );
          }}
        </Formik>
      </div>
      <Footer />
    </div>
  );
};
export default UserPage;
