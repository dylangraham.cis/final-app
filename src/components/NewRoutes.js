import React, { Component } from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import LoginPage from "./users/LoginPage";
import UserPage from "./users/UserPage";
import Table from "./users/Table";

const AuthRoute = ({ component: Component, ...rest }) => {
  const hasAuthAccessToken = localStorage.getItem("token");
  return (
    <Route
      {...rest}
      //rest for another 2 props
      render={(props) => {
        return hasAuthAccessToken ? (
          <Component {...props} />
        ) : (
          <Redirect to="/" />
        );
      }}
    />
  );
};

export default class NewRoutes extends Component {
  render() {
    return (
      <Switch>
        <Route exact name="login" path="/" component={LoginPage} />
        {/* <Route exact name="edit" path="/users" component={Table} /> */}

        <AuthRoute
          exact
          name="HomePage"
          path="/users/add"
          component={UserPage}
        />
        <AuthRoute exact name="HomePage" path="/users" component={Table} />
      </Switch>
    );
  }
}
