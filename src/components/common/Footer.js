import React from "react";

const Footer = () => {
  return (
    <div>
      <div className="footer">
        <section id="footer">
          <div className="footer" style={{ marginLeft: 90 }}>
            <div className="brand">
              <h2>Dylan Graham</h2>
            </div>
            <h2>Your Complete Web Solution</h2>
            <div className="social-icon">
              <div className="social-item">
                <a href="#">
                  <img src="https://img.icons8.com/bubbles/100/000000/facebook-new.png" />
                </a>
              </div>
              <div className="social-item">
                <a href="#">
                  <img src="https://img.icons8.com/bubbles/100/000000/instagram-new.png" />
                </a>
              </div>
              <div className="social-item">
                <a href="#">
                  <img src="https://img.icons8.com/bubbles/100/000000/twitter.png" />
                </a>
              </div>
              <div className="social-item">
                <a href="#">
                  <img src="https://img.icons8.com/bubbles/100/000000/behance.png" />
                </a>
              </div>
            </div>
            <p>React Redux</p>
          </div>
        </section>
      </div>
    </div>
  );
};

export default Footer;
