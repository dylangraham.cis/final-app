import React from "react";
import { Link, useLocation } from "react-router-dom";
import { useHistory } from "react-router-dom/cjs/react-router-dom.min";

const Navbar = () => {
  const location = useLocation();
  const history = useHistory();

  const routeNames = [
    { name: "Add new", redirectTo: "/users/add" },
    { name: "Users data", redirectTo: "/users" },
  ];

  const handleLogout = () => {
    localStorage.removeItem("token");
    history.push("/");
  };

  return (
    <div className="navbar" style={{ marginBottom: 40 }}>
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <div
          className="collapse navbar-collapse"
          id="navbarSupportedContent"
          style={{ justifyContent: "space-Between" }}
        >
          <ul className="navbar-nav mr-auto">
            {routeNames.map((d) => {
              return (
                <li className="nav-item">
                  <Link
                    to={d.redirectTo}
                    className={
                      d.redirectTo === location.pathname
                        ? "nav-link active"
                        : "nav-link"
                    }
                  >
                    {d.name}
                  </Link>
                </li>
              );
            })}
            <li className="nav-item">
              <button
                className="dropdown-item"
                id="logoutbutton"
                onClick={() => {
                  handleLogout();
                }}
              >
                Logout
              </button>
            </li>
          </ul>
        </div>
      </nav>
    </div>
  );
};

export default Navbar;
