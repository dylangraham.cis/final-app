import React from "react";
import { Field, ErrorMessage } from "formik";
import Select from "react-select";

function SelectOptions(props) {
  const { label, name, options, ...rest } = props;
  return (
    <div>
      <label htmlFor={name}>{label}</label>
      <Field name={name}>
        {({ form }) => {
          const { setFieldValue } = form;
          return (
            <Select
              id={name}
              name={name}
              {...rest}
              onChange={(values) => setFieldValue(name, values.label)}
              options={options}
            />
          );
        }}
      </Field>
      <ErrorMessage name={name} />
    </div>
  );
}

export default SelectOptions;
