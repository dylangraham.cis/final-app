import React, { Component, useEffect, useState } from "react";
import "./App.css";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Provider } from "react-redux";
import reduxThunk from "redux-thunk";
import { createStore, applyMiddleware } from "redux";
import rootReducer from "./redux/index";
import NewRoutes from "./components/NewRoutes";
import { BrowserRouter as Router } from "react-router-dom";

const createStoreWithMiddleware = applyMiddleware(reduxThunk)(createStore);

//createStoreWithMiddleware return function in react thunk

const store = createStoreWithMiddleware(rootReducer);

const App = () => {
  return (
    <div>
      <Provider store={store}>
        <Router>
          <NewRoutes />
        </Router>
      </Provider>
      <ToastContainer />
    </div>
  );
};

export default App;
