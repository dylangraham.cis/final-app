import {
  USER_LIST_TYPE_LOADING,
  USER_LIST_TYPE_SUCCESS,
  USER_LIST_TYPE_FAILURE,
} from "../type/user";
const userList = (
  state = {
    data: null,
    error: null,
    isLoading: false,
  },
  action
) => {
  switch (action.type) {
    case USER_LIST_TYPE_LOADING:
      return {
        ...state,
        isLoading: true,
      };
    case USER_LIST_TYPE_SUCCESS:
      return {
        isLoading: false,
        data: action.Data,
        error: "",
      };
    case USER_LIST_TYPE_FAILURE:
      return {
        isLoading: false,
        data: [],
        error: action.error,
      };
    default:
      return state;
  }
};
export default userList;
