import {
  DELETE_USER_TYPE_SUCCESS,
  DELETE_USER_TYPE_LOADING,
  DELETE_USER_TYPE_FAILURE,
} from "../type/user";

const deleteUser = (
  state = {
    data: null,
    error: null,
    isLoading: false,
  },
  action
) => {
  switch (action.type) {
    case DELETE_USER_TYPE_LOADING:
      return {
        ...state,
        isLoading: true,
      };
    case DELETE_USER_TYPE_SUCCESS:
      return {
        isLoading: false,
        data: action.data,
        error: null,
      };
    case DELETE_USER_TYPE_FAILURE:
      return {
        isLoading: false,
        data: null,
        error: "wrong api",
      };

    default:
      return state;
  }
};
export default deleteUser;
