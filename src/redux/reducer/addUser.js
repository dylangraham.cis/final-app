import {
  ADD_USER_TYPE_SUCCESS,
  ADD_USER_TYPE_LOADING,
  ADD_USER_TYPE_FAILURE,
} from "../type/user";

const addUser = (
  state = {
    data: null,
    error: null,
    isLoading: false,
  },
  action
) => {
  switch (action.type) {
    case ADD_USER_TYPE_LOADING:
      return {
        ...state,
        isLoading: true,
      };
    case ADD_USER_TYPE_SUCCESS:
      return {
        isLoading: false,
        data: action.data,
        error: null,
      };
    case ADD_USER_TYPE_FAILURE:
      return {
        isLoading: false,
        data: null,
        error: "wrong api",
      };

    default:
      return state;
  }
};
export default addUser;
