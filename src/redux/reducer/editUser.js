import {
  EDIT_USER_TYPE_SUCCESS,
  EDIT_USER_TYPE_LOADING,
  EDIT_USER_TYPE_FAILURE,
} from "../type/user";

const editUser = (
  state = {
    data: null,
    error: null,
    isLoading: false,
  },
  action
) => {
  switch (action.type) {
    case EDIT_USER_TYPE_LOADING:
      return {
        ...state,
        isLoading: true,
      };
    case EDIT_USER_TYPE_SUCCESS:
      return {
        isLoading: false,
        data: action.data,
        error: null,
      };
    case EDIT_USER_TYPE_FAILURE:
      return {
        isLoading: false,
        data: null,
        error: "wrong api",
      };

    default:
      return state;
  }
};
export default editUser;
