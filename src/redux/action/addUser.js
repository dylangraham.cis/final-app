import {
  ADD_USER_TYPE_SUCCESS,
  ADD_USER_TYPE_LOADING,
  ADD_USER_TYPE_FAILURE,
} from "../type/user";
import { postApi } from "../api";

export const addUserLoading = () => {
  return {
    type: ADD_USER_TYPE_LOADING,
  };
};
export const addUserSuccess = (Data) => {
  // console.log("~ file: addAction.js ~ line 11 ~ addUserSuccess ~ Data", Data);
  return {
    type: ADD_USER_TYPE_SUCCESS,
    Data,
  };
};
export const addUserFailure = (error) => {
  return {
    type: ADD_USER_TYPE_FAILURE,
    error,
  };
};
export const addUser = (userData) => (dispatch) => {
  dispatch(addUserLoading());
  return postApi("/user", userData)
    .then((Data) => {
      dispatch(addUserSuccess(Data));
      return Data?.Data?.responseData ?? Data?.Data ?? Data ?? null;
    })
    .catch((error) => {
      dispatch(addUserFailure(error));
    });
};
