import {
  DELETE_USER_TYPE_SUCCESS,
  DELETE_USER_TYPE_LOADING,
  DELETE_USER_TYPE_FAILURE,
} from "../type/user";
import { deleteApi } from "../api";

export const deleteUserLoading = () => ({
  type: DELETE_USER_TYPE_LOADING,
});

export const deleteUserSuccess = (Data) => ({
  type: DELETE_USER_TYPE_SUCCESS,
  Data,
});

export const deleteUserFailure = (error) => ({
  type: DELETE_USER_TYPE_FAILURE,
  error,
});

export const deleteUser = (id) => (dispatch) => {
  dispatch(deleteUserLoading());

  return deleteApi(`/user/${id}`)
    .then((Data) => {
      dispatch(deleteUserSuccess(Data));
    })
    .catch((error) => {
      if (error) {
        alert(error, "error");
      }
      dispatch(deleteUserFailure(error));
    });
};
