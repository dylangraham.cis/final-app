import {
  USER_LIST_TYPE_LOADING,
  USER_LIST_TYPE_SUCCESS,
  USER_LIST_TYPE_FAILURE,
} from "../type/user";
import { getApi } from "../api";

export const userListLoading = () => ({
  type: USER_LIST_TYPE_LOADING,
});

export const userListSuccess = (Data) => ({
  type: USER_LIST_TYPE_SUCCESS,
  Data,
});
export const userListFailure = (error) => ({
  type: USER_LIST_TYPE_FAILURE,
  error,
});

export const userList = (userData) => (dispatch) => {
  dispatch(userListLoading());

  return getApi("/user", userData)
    .then((Data) => {
      dispatch(userListSuccess(Data));
      return Data?.Data?.responseData ?? Data?.Data ?? Data ?? null;
    })
    .catch((error) => {
      dispatch(userListFailure(error));
    });
};
