import {
  EDIT_USER_TYPE_SUCCESS,
  EDIT_USER_TYPE_LOADING,
  EDIT_USER_TYPE_FAILURE,
} from "../type/user";
import { putApi } from "../api";
import { toast } from "react-toastify";

export const editUserLoading = () => ({
  type: EDIT_USER_TYPE_LOADING,
});

export const editUserSuccess = (Data) => ({
  type: EDIT_USER_TYPE_SUCCESS,
  Data,
});

export const editUserFailure = (error) => ({
  type: EDIT_USER_TYPE_FAILURE,
  error,
});

export const updateList = (id, userData) => (dispatch) => {
  // console.log(11111111, userData);
  dispatch(editUserLoading());
  return putApi(`/user/${id}`, userData)
    .then((Data) => {
      dispatch(editUserSuccess(userData));
      return Data?.Data?.responseData ?? Data?.Data ?? Data ?? null;
    })
    .catch((error) => {
      if (error) {
        toast(error, "error");
      }
      dispatch(editUserFailure(error));
    });
};
