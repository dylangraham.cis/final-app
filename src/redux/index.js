import { combineReducers } from "redux";
import addUser from "./reducer/addUser";
import deleteUser from "./reducer/deleteUser";
import editUser from "./reducer/editUser";
import userList from "./reducer/userList";

const rootReducer = combineReducers({
  //right side isx reducer name from reducer file and left side for selector

  addUser: addUser,
  userList: userList,
  deleteUser: deleteUser,
  editUser: editUser,
});
export default rootReducer;
