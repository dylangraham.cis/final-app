import axios from "axios";
import { apiEnd } from "./type/apiEnd";

export const getApi = (url, params) => {
  return axios.get(apiEnd + url, params);
};

export const postApi = (url, apiData) => {
  return axios.post(apiEnd + url, apiData);
};
export const putApi = (url, apiData) => {
  return axios.put(apiEnd + url, apiData);
};

export const deleteApi = (url, apiData) => {
  return axios.delete(apiEnd + url, apiData);
};
